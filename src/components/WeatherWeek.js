import React, { Component } from "react";

import '../styles/WeatherWeek.css';

class WeatherWeek extends Component {

  render() {
    const weatherData = this.props.weatherData;

    if (!this.props.hidden)
      return  <div></div>;
    return (
      <div className='tableWeek'>
        {console.log(weatherData)}
        <div className="table-title">
          <h3>Weather for the week</h3>
        </div>
        <table className="table-fill">
          <thead>
          <tr>
            <th className="text-left">

            </th>
            {weatherData.map((item, i) =>
              <th className="text-left" key={i}>
                {new Date(item.date).toLocaleString('en', {weekday: 'short'})+ ' '}
                {new Date(item.date).getDate()}
              </th>
            )}
          </tr>
          </thead>
          <tbody className="table-hover">
          <tr>
            <td className="text-left">
              Icon
            </td>
            {weatherData.map((item, i) =>
              <td className="text-left" key={i}>
                <img src={item.day.condition.icon} alt=""/>
              </td>
            )}
          </tr>
          <tr>
            <td className="text-left">
              Temp
            </td>
            {weatherData.map((item, i) =>
              <td className="text-left" key={i}>
                {item.day.avgtemp_c}
              </td>
            )}
          </tr>
          <tr>
            <td className="text-left">
              Wind
            </td>
            {weatherData.map((item, i) =>
              <td className="text-left" key={i}>
                {item.day.avgvis_miles}
              </td>
            )}
          </tr>
          <tr>
            <td className="text-left">
              Humidity
            </td>
            {weatherData.map((item, i) =>
              <td className="text-left" key={i}>
                {item.day.avghumidity}%
              </td>
            )}
          </tr>
          <tr>
            <td className="text-left">
              Precip
            </td>
            {weatherData.map((item, i) =>
              <td className="text-left" key={i}>
                {item.day.totalprecip_in}%
              </td>
            )}
          </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default WeatherWeek;