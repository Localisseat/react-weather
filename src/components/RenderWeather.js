import React, { Component } from "react";
import axios from 'axios';
import WeatherWeek from "./WeatherWeek";

import '../styles/RenderWeather.css';

class RenderWeather extends Component {
  constructor() {
    super();
    this.state = {
      weatherData: null,
      isHidden: false,
      autoIp: false
    };
  }

  componentDidUpdate(prevProps, prevState) {
      if (this.props.countries !== prevProps.countries) {
        axios.get(`http://api.apixu.com/v1/forecast.json?key=939176055d8b4401a4a50606191803&q=${this.props.countries}&days=7`)
          .then(response => this.setState({ weatherData: response.data}))
            .catch(function (error) {
              console.log(error);
            })
      }

      if (this.state.autoIp !== prevState.autoIp) {
        axios.get(`http://api.apixu.com/v1/forecast.json?key=939176055d8b4401a4a50606191803&q=auto:ip&days=7`)
          .then(response => this.setState({ weatherData: response.data}))
          .catch(function (error) {
            console.log(error);
          })
      }
  }

  toggleIsHidden () {
    this.setState((currentState) => ({
      isHidden: !currentState.isHidden
    }));
  }

  toggleIsAutoIp () {
    this.setState((currentState) => ({
      autoIp: !currentState.autoIp
    }));
  }

  render() {
    const weatherData = this.state.weatherData;

    if (!weatherData)
      return  <div className="btn-position btn-container">
                <input onClick={() => this.toggleIsAutoIp()} type="submit" value="Auto_find" id="button-blue"/>
              </div>;
    return (
      <div className='containerWeather'>

        <div id="wrapper">
          <section>
            <div className="btn-container">
              <input onClick={() => this.toggleIsAutoIp()} type="submit" value="Auto_find" id="button-blue"/>
              <input onClick={() => this.toggleIsHidden()} type="submit" value="More_info" id="button-blue"/>
            </div>
          </section>
        </div>

        <div className='location'>{weatherData.location.country}</div>
        <div className='regionCapital'>{weatherData.location.region} {weatherData.location.name}</div>
        <div>{weatherData.location.localtime}</div>

          {this.state.isHidden &&
          <div>
            Lat: {weatherData.location.lat}
            Lon: {weatherData.location.lon}
          </div>}

          {!this.state.isHidden &&
          <div className='temp'>
            {weatherData.current.temp_c}°C
          </div>}
          {this.state.isHidden &&
          <div className='temp'>
            {weatherData.current.temp_f}°F
          </div>}

        <img src={weatherData.current.condition.icon} alt="weather" />
        <div>{weatherData.current.condition.text}</div>
        <div>Wind: {weatherData.current.wind_mph} MHP</div>

          {this.state.isHidden &&
          <div>
            Wind: {weatherData.current.wind_kph} KPH
          </div>}
        <div>Humidity {weatherData.current.humidity}%</div>
        <div>Cloudiness {weatherData.current.cloud}%</div>
        {this.state.isHidden &&
        <div>
          Pressure: {weatherData.current.pressure_mb} MB
        </div>}
        {!this.state.isHidden &&
        <div>
          Feelslike: {weatherData.current.feelslike_c}°C
        </div>}
        {this.state.isHidden &&
        <div>
        Feelslike: {weatherData.current.feelslike_f}°F
        </div>}

        <WeatherWeek
          hidden={this.state.isHidden}
          weatherData={weatherData.forecast.forecastday}
        />

      </div>
    );
  }
}

export default RenderWeather;