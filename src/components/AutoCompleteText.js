import React, { Component } from "react";
import RenderWeather from './RenderWeather';

import '../styles/AutoCompleteText.css';

class AutoCompleteText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestions: [],
      text: '',
      countries: '',
    };
  }

  onTextChanged = (e) => {
    const {item} = this.props;
    const value = e.target.value;
    let suggestions = [];
    if (value.length > 0) {
      const regex = new RegExp(`^${value}`, 'i');
      suggestions = item.sort().filter(v => regex.test(v));
    }
    this.setState(() => ({ suggestions, text: value }));
  };

  suggestionSelected (value) {
    this.setState(() => ({
      text: value,
      suggestions: [],
      countries: value,
    }));
  }

  renderSuggestions () {
    const { suggestions } = this.state;
    if (suggestions.length === 0) {
      return null;
    }
    return (
      <ul>
        {suggestions.map((item, i) => <li key={i} onClick={() => this.suggestionSelected(item)}> {item}</li>)}
      </ul>
    );
  }

  render() {
    const { text, countries } = this.state;
    return (
        <React.Fragment>
          <div className='AutoCompleteText'>
            <input value={ text } onChange={this.onTextChanged} type="text"/>
            {this.renderSuggestions()}
          </div>
            <RenderWeather countries = { countries }/>
        </React.Fragment>
    );
  }
}

export default AutoCompleteText;