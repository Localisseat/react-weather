import React, { Component } from "react";
import AutoCompleteText from './AutoCompleteText';
import Countries from '../countries';

import '../styles/App.css';

class App extends Component {
  render() {
    return (
        <div className='App-Component'>
            <AutoCompleteText item={Countries}/>
        </div>
    );
  }
}

export default App;